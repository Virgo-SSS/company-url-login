<?php

namespace App\Http\Controllers\Auth;

use App\Models\employee;
use App\Models\companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $companie = companies::select('id', 'company_name');
        $companie = $companie->where('company_name', $request->companies)->first();

        if(empty($companie)){
            $errors = ['companyerror' => 'Company not found'];
            return back()->withErrors($errors);
        }

        $employee = employee::with(['Rcompany' => function($query){
            $query->select('id', 'company_name');
        }]);
        $employee = $employee->select('id','name', 'password', 'company_id', 'city', 'phone', 'email');
        $employee = $employee->where('email', $request->email)
                            ->where('company_id', $companie->id)
                            ->first();

        if(empty($employee)) {
            $errors = ['email' => 'invalid Email'];
            return back()->withErrors($errors);
        }
    
        if(!Hash::check($request->get('password'), $employee->password)) {
            $errors = ['password' => 'invalid password'];
            return back()->withErrors($errors);
        }

        Auth::login($employee);

        Session::put('companyname', $employee->Rcompany->company_name);
        return redirect()->route('company', $employee->Rcompany->company_name);
    }
}
