<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\companies;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class employee extends Authenticatable
{
    use HasFactory;
    // make protected
    
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'password',
        'company_id',
        'city',
        'phone',
        'email',
        'created_users_id',
        'updated_users_id',
       
    ];


    public function Rcompany()
    {
        return $this->belongsTo(companies::class, 'company_id')->withDefault();
    }

    // public function Rsell()
    // {
    //     return $this->hasMany(sell::class);
    // }

    // public function Rsummaries()
    // {
    //     return $this->hasMany(sellsummaries::class);
    // }

   


    
   
   
}
