@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Employee</div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Company</th>
                                <th scope="col">City</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employee as $emp)
                                
                            <tr>
                                <td scope="row">{{ $emp->name }}</td>
                                <td>{{ $emp->Rcompany->company_name }}</td>
                                <td>{{ $emp->city }}</td>
                                <td>{{ $emp->phone }}</td>
                                <td>{{ $emp->email }}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    {{ $employee->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
